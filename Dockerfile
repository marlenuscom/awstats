FROM httpd:2.4.54-alpine
LABEL org.opencontainers.image.authors="marlenuscom@gmail.com"

ARG MOD_PERL_VERSION=2.0.12
ARG MOD_PERL_SHA=f5b821b59b0fdc9670e46ed0fcf32d8911f25126189a8b68c1652f9221eee269
ARG NAMESPACE
ENV NAMESPACE=$NAMESPACE

ENV AWSTATS_CONF_LOGFILE=/var/log/nginx/access.log
ENV AWSTATS_CONF_LOGFORMAT="%host %other %logname %time1 %methodurl %code %bytesd %refererquot %uaquot"
ENV AWSTATS_CONF_SITEDOMAIN=localhost
ENV AWSTATS_CONF_HOSTALIASES="localhost 127.0.0.1 REGEX[^.*\.domain\.ru$]"
ENV AWSTATS_SKIP_HOSTS=""
ENV AWSTATS_CONF_INCLUDE="."

RUN apk add --no-cache gettext awstats tzdata \
    && apk add --no-cache --virtual .build-dependencies apr-dev apr-util-dev gcc libc-dev make wget perl-dev \
    && cd /tmp \
    && wget https://www-eu.apache.org/dist/perl/mod_perl-${MOD_PERL_VERSION}.tar.gz \
    && echo "${MOD_PERL_SHA}  mod_perl-${MOD_PERL_VERSION}.tar.gz" | sha256sum -c \
    && tar xf mod_perl-${MOD_PERL_VERSION}.tar.gz \
    && cd mod_perl-${MOD_PERL_VERSION} \
    && perl Makefile.PL MP_APXS=/usr/local/apache2/bin/apxs MP_APR_CONFIG=/usr/bin/apr-1-config --cflags --cppflags --includes \
    && make -j4 \
    && mv src/modules/perl/mod_perl.so /usr/local/apache2/modules/ \
    && echo 'ServerName localhost:80' >> /usr/local/apache2/conf/httpd.conf \
    && echo 'LoadModule perl_module modules/mod_perl.so' >> /usr/local/apache2/conf/httpd.conf \
    && echo 'Include conf/awstats_httpd.conf' >> /usr/local/apache2/conf/httpd.conf \
    && cd .. \
    && rm -rf ./mod_perl-${MOD_PERL_VERSION}* \
    && wget https://raw.githubusercontent.com/miyagawa/cpanminus/master/cpanm \
    && chmod +x cpanm \
    && ./cpanm Geo::IP \
    && rm -rf ./cpanm /root/.cpanm \
    && apk del --no-cache .build-dependencies

COPY entrypoint.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/entrypoint.sh
COPY GeoIP /etc/awstats/GeoIP
COPY awstats_env.conf /etc/awstats/
COPY awstats_httpd.conf /usr/local/apache2/conf/

VOLUME ["/var/lib/awstats/"]

EXPOSE 80

ENTRYPOINT ["entrypoint.sh"]
CMD ["httpd-foreground"]
