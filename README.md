# Awstats

## Quickstart

```bash
docker run \
  --detach \
  --name awstats \
  --restart always \
  --publish 8080:80 \
  --volume awstats:/var/lib/awstats \
  --volume ${PWD}/logs/nginx:/var/log/nginx:ro \
  -e TZ=Europe/Moscow \
  -e AWSTATS_SKIP_HOSTS="" \
  -e AWSTATS_CONF_SITEDOMAIN=dev.domain.ru \
  -e AWSTATS_CONF_LOGFILE=/var/log/nginx/dev.access.log \
  -e AWSTATS_CONF_HOSTALIASES="localhost 127.0.0.1 REGEX[^.*\.domain\.ru$]" \
  registry.gitlab.com/marlenuscom/awstats/awstats:build_test
```

```
docker exec awstats awstats_updateall.pl now
```

## Configuration

The configuration file `/etc/awstats/awstats.conf` will be automatically generated at runtime.
The generated configuration can be modified using the following optional environment variables, see `entrypoint.sh` for defaults.
- `AWSTATS_CONF_LOGFILE`: Log file
- `AWSTATS_CONF_LOGFORMAT`: Set the `LogFormat` value. If quotes are desired, they should be added by the caller: `-e AWSTATS_CONF_LOGFORMAT=\"...\"`
- `AWSTATS_CONF_SITEDOMAIN`: The site domain, default `localhost`.
  If you change this you must set the `config=` query parameter when using the web interface, e.g. http://localhost/awstats/awstats.pl?config=localhost (this is intended to support statistics for multiple domains)
- `AWSTATS_CONF_HOSTALIASES`: A space separated list of site aliases.
- `AWSTATS_SKIP_HOSTS`: A space separated list of regex IP matches to be skipped.
- `TZ`: Time zone for the docker container
